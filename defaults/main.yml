---
# defaults file for powerdns
base_image: 'ubuntu:20.04'
powerdns_pihole_enabled: true
powerdns_pihole_virtual_host: pihole.example.net
powerdns_pihole_webpassword: admin
powerdns_pihole_timezone: UTC
powerdns_folder: /opt/powerdns
powerdns_api_key: myrandomkey
powerdns_db_root_password: r00t!sC00l
powerdns_dnsdist_bind_address: '{{ ansible_default_ipv4.address }}'
powerdns_dnsdist_bind_interface: '{{ ansible_default_ipv4.interface }}'
powerdns_db: powerdns
powerdns_db_user: powerdns
powerdns_db_password: my_secret_Passw0rd
powerdns_publish_api: true
powerdns_traefik_deployment: false
powerdns_loglevel: 4
powerdns_webserver_loglevel: normal
powerdns_ipv4_cidr: 10.0.2.0/24
powerdns_ipv6_cidr: fd00::/8
powerdns_recursor_api_host: '{{ ansible_default_ipv4.address }}'
powerdns_recursor_api_port: '{% if powerdns_traefik_deployment %}80{% else %}8082{% endif %}'
powerdns_api_host: '{{ ansible_default_ipv4.address }}'
powerdns_api_port: '{% if powerdns_traefik_deployment %}80{% else %}8081{% endif %}'
powerdns_server_api_over_ssl: false
powerdns_server_api_ssl_validate_certs: 'yes'
powerdns_recursor_api_over_ssl: false
powerdns_recursor_api_ssl_validate_certs: 'yes'
powerdns_server_traefik_labels:
  traefik.enable: true
  traefik.http.routers.powerdns-powerdns.entrypoints: http
  traefik.http.routers.powerdns-powerdns.rule: Host(`{{ powerdns_zones[0].nameservers[0] }}`)
  traefik.http.services.powerdns-powerdns.loadbalancer.server.port: 8081
powerdns_recursor_traefik_labels:
  traefik.enable: true
  traefik.http.routers.powerdns-recursor.entrypoints: http
  traefik.http.routers.powerdns-recursor.rule: "Host(`{{ powerdns_zones[0].nameservers[0] }}`) && PathPrefix(`/recursor/`)"
  traefik.http.services.powerdns-recursor.loadbalancer.server.port: 8081
  traefik.http.middlewares.powerdns-recursor-replace.replacepathregex.regex: ^/recursor/(.*)
  traefik.http.middlewares.powerdns-recursor-replace.replacepathregex.replacement: /$$1
  traefik.http.routers.powerdns-recursor.middlewares: powerdns-recursor-replace
powerdns_pihole_traefik_labels:
  traefik.enable: true
  traefik.http.routers.powerdns-pihole.entrypoints: http
  traefik.http.routers.powerdns-pihole.rule: "Host(`{{ powerdns_pihole_virtual_host }}`)"
  traefik.http.services.powerdns-pihole.loadbalancer.server.port: 80
powerdns_environment: {}
powerdns_recursor_environment: {}
powerdns_server_flags:
  - --no-config
  - --daemon=no
  - --launch=gmysql
  - --gmysql-host=mariadb
  - --gmysql-dbname={{ powerdns_db }}
  - --gmysql-user={{ powerdns_db_user }}
  - --gmysql-password={{ powerdns_db_password }}
  - --loglevel={{ powerdns_loglevel }}
  - --webserver
  - --webserver-address=0.0.0.0
  - --webserver-port=8081
  - --webserver-allow-from=127.0.0.0/8,10.0.0.0/8,169.254.0.0/16,172.16.0.0/12,192.168.0.0/16
  - --webserver-loglevel={{ powerdns_webserver_loglevel }}
  - --api=yes
  - --api-key={{ powerdns_api_key }}
  - --master=yes
  - --local-port=53
  - --local-ipv6=
powerdns_recursor_flags:
  - --local-address=192.168.222.40
  - --local-port=53
  - --loglevel={{ powerdns_loglevel }}
  - --webserver=yes
  - --webserver-address=0.0.0.0
  - --webserver-port=8081
  - --webserver-loglevel={{ powerdns_webserver_loglevel }}
  - --api-key={{ powerdns_api_key }}
  - --forward-zones-file=/etc/powerdns/forward_zones
  - --allow-from=127.0.0.0/8,10.0.0.0/8,169.254.0.0/16,172.16.0.0/12,192.168.0.0/16
  - --security-poll-suffix=""
powerdns_zones:
  - name: example.net.
    type: zone
    kind: Native
    nameservers:
      - ns1.example.net.
    rrsets:
      - name: example.net.
        type: SOA
        ttl: 86400
        records:
          - content: ns1.example.net. hostmaster.example.com. 1 1800 900 604800 86400
            disabled: false
      - name: ns1.example.net.
        type: A
        ttl: 3600
        records:
          - content: '{{ ansible_default_ipv4.address }}'
            disabled: false
      - name: www.example.net.
        type: A
        ttl: 3600
        records:
          - content: 192.168.1.42
            disabled: false
      - name: "{{ powerdns_pihole_virtual_host }}."
        type: A
        ttl: 3600
        records:
          - content: '{{ ansible_default_ipv4.address }}'
            disabled: false
      - name: alpha.example.net.
        type: A
        ttl: 3600
        records:
          - content: 192.168.1.42
            disabled: false
      - name: beta.example.net.
        type: A
        ttl: 3600
        records:
          - content: 192.168.1.50
            disabled: false
  - name: hase.de.
    type: zone
    kind: Native
    nameservers:
      - ns1.hase.de.
    rrsets:
      - name: hase.de.
        type: SOA
        ttl: 86400
        records:
          - content: ns.hase.de. hostmaster.example.com. 1 1800 900 604800 86400
            disabled: false
      - name: www.hase.de.
        type: A
        ttl: 3600
        records:
          - content: '{{ ansible_default_ipv4.address }}'
            disabled: false
      - name: www1.hase.de.
        type: A
        ttl: 1800
        records:
          - content: '{{ ansible_default_ipv4.address }}'
            disabled: false
