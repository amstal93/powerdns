---
# This is an example playbook to execute Ansible tests.
- name: Verify
  hosts: all
  tasks:
  - name: docker-compose.yml should not be accessable by others
    register: result
    file:
      path: /opt/powerdns/docker-compose.yml
      state: touch
    failed_when: result.msg is not search("Permission denied")

  - name: check for healthy powerdns containers
    become: 'yes'
    register: result
    command: docker ps
    failed_when: result.stdout is not search("Up.+\(healthy\).+powerdns_"+item)
    changed_when: false
    with_items:
      - db
      - server
      - recursor
      - dnsdist

  - name: check for listening dns ports
    register: result
    become: 'yes'
    command: ss -tulpen
    changed_when: false
    failed_when: result is not search('(udp|tcp)\s+(LISTEN|UNCONN).+10.0.2.15:53')

  - name: ensure powerdns-server api not unauth accessible
    become: 'yes'
    register: 'result'
    uri:
      url: 'http://{{ ansible_default_ipv4.address }}:8081/api/v1/servers'
      method: GET
      return_content: 'yes'
    failed_when: result.status != 401

  - name: ensure powerdns-recursor api not unauth accessible
    become: 'yes'
    register: 'result'
    uri:
      url: 'http://{{ ansible_default_ipv4.address }}:8082/api/v1/servers'
      method: GET
      return_content: 'yes'
    failed_when: result.status != 401

  - name: check powerdns-server api endpoint zones
    become: 'yes'
    register: 'result'
    uri:
      url: 'http://{{ ansible_default_ipv4.address }}:8081/api/v1/servers/localhost/zones'
      headers:
        X-API-Key: 'myrandomkey'
      method: GET
      return_content: 'yes'

  - name: example.net should be Native
    assert:
      that: (result.json | selectattr("name", "eq", "example.net.") | first ).kind == "Native"

  - name: check powerdns-recursor api endpoint zones
    become: 'yes'
    register: 'result'
    uri:
      url: 'http://{{ ansible_default_ipv4.address }}:8082/api/v1/servers/localhost/zones'
      headers:
        X-API-Key: 'myrandomkey'
      method: GET
      return_content: 'yes'

  - name: example.net should be Forwarded
    assert:
      that: (result.json | selectattr("name", "eq", "example.net.") | first ).kind == "Forwarded"

  - name: resolve authorativ www.example.net on host
    become: 'yes'
    register: result
    command: dig www.example.net
    failed_when: result.stdout is not search('www\.example\.net.+192\.168\.1\.42')
    changed_when: false

  - name: resolve recursive www.archive.org on host
    become: 'yes'
    register: result
    command: dig www.archive.org
    changed_when: false
    failed_when: result.stdout is not search('www\.archive\.org\..+\d+\.\d+\.\d+\.\d+')

  - name: test resolving in container on default bridge
    become: 'yes'
    register: result
    command: docker run praqma/network-multitool nslookup www.google.de
    changed_when: false
    failed_when: result.stdout is search('connection timed out; no servers could be reached')

  - name: test resolving in dnsdist container
    become: 'yes'
    register: result
    command: docker exec powerdns_dnsdist nslookup www.google.de
    changed_when: false
    failed_when: result.stdout is search('connection timed out; no servers could be reached')

  - name: test resolving in container with custom network
    become: 'yes'
    register: result
    command: docker run --network powerdns_dns_net praqma/network-multitool nslookup www.google.de
    changed_when: false
    failed_when: result.stdout is search('connection timed out; no servers could be reached')
  - debug:
      msg: '{{ result.stdout }}'

  - name: test pihole is aktive
    become: 'yes'
    register: result
    changed_when: false
    command: nslookup pi.hole

  - name: test pihole is filtering
    become: 'yes'
    register: result
    command: nslookup flurry.com
    changed_when: false
    failed_when: result.stdout is not search('0.0.0.0')
